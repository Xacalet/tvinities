package com.alexbarcelo.tvinities.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alexbarcelo.tvinities.R;
import com.alexbarcelo.tvinities.di.ActivityScoped;
import com.alexbarcelo.tvinities.glide.GlideApp;
import com.alexbarcelo.tvinities.model.TVShow;

import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.alexbarcelo.tvinities.api.TVinitiesAPI.BACKDROP_IMAGES_BASE_URL;

@ActivityScoped
public class TVShowListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private TVShowListAdapter.Presenter mPresenter;
    private OnItemClickListener mOnItemClickListener;
    private ItemType mExtraItemType = ItemType.NONE;

    @Inject
    TVShowListAdapter(Context context, TVShowListAdapter.Presenter presenter) {
        mContext = context;
        mPresenter = presenter;
    }

    @Override
    @NonNull
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == ItemType.TV_SHOW.ViewType) {
            return new ShowItemViewHolder(inflater.inflate(R.layout.tv_show_list_item, parent, false));
        } else if (viewType == ItemType.LOADING_SPINNER.ViewType) {
            return new LoadingItemViewHolder(inflater.inflate(R.layout.loading_spinner_list_item, parent, false));
        } else if (viewType == ItemType.RETRY_BUTTON.ViewType) {
            return new RetryItemViewHolder(inflater.inflate(R.layout.retry_button_list_item, parent, false));
        } else {
            throw new IllegalArgumentException("Unknown view type");
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == ItemType.TV_SHOW.ViewType) {
            ShowItemViewHolder vh = (ShowItemViewHolder) holder;

            final TVShow item = mPresenter.getShowByPosition(position);

            vh.mName.setText(item.name());

            vh.mLayout.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onTVShowClick(item.id());
                }
            });

            Double voteRating = item.voteAverage();
            vh.mRating.setText(String.format(Locale.getDefault(), "%.1f", voteRating));
            Drawable drawable = DrawableCompat.wrap(vh.mRating.getBackground());
            if (voteRating >= 7.) {
                DrawableCompat.setTint(drawable, mContext.getResources().getColor(android.R.color.holo_green_dark));
            } else if (voteRating >= 5.) {
                DrawableCompat.setTint(drawable, mContext.getResources().getColor(android.R.color.holo_orange_dark));
            } else {
                DrawableCompat.setTint(drawable, mContext.getResources().getColor(android.R.color.holo_red_light));
            }

            if (item.posterPath() != null) {
                GlideApp.with(mContext)
                        .load(BACKDROP_IMAGES_BASE_URL + item.posterPath())
                        .placeholder(R.drawable.no_poster_found)
                        .into(vh.mPoster);
            }

        } else if (holder.getItemViewType() == ItemType.RETRY_BUTTON.ViewType) {
            RetryItemViewHolder vh = (RetryItemViewHolder) holder;
            vh.mRetryButton.setOnClickListener(view -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onRetryButtonClick();
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position != getItemCount() - 1 || mExtraItemType == ItemType.NONE) {
            return ItemType.TV_SHOW.ViewType;
        } else {
            return mExtraItemType.ViewType;
        }
    }

    @Override
    public int getItemCount() {
        return mPresenter.getItemCount() + ((mExtraItemType == ItemType.NONE) ? 0 : 1);
    }

    public void showRetryButtonItem() {
        mExtraItemType = ItemType.RETRY_BUTTON;
        notifyItemChanged(getItemCount() - 1);
    }

    public void showLoadingSpinnerItem() {
        mExtraItemType = ItemType.LOADING_SPINNER;
        notifyItemChanged(getItemCount() - 1);
    }

    public void hideRetryButtonItem() {
        mExtraItemType = ItemType.NONE;
        notifyItemChanged(getItemCount() - 1);
    }

    public void hideLoadingSpinnerItem() {
        mExtraItemType = ItemType.NONE;
        notifyItemChanged(getItemCount() - 1);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    public boolean isRetryButtonDisplayed() {
        return mExtraItemType == ItemType.RETRY_BUTTON;
    }

    class ShowItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.show_list_item_layout) RelativeLayout mLayout;
        @BindView(R.id.show_list_item_name) TextView mName;
        @BindView(R.id.show_list_item_rating) TextView mRating;
        @BindView(R.id.show_list_item_poster) ImageView mPoster;

        ShowItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class RetryItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.retry_button) Button mRetryButton;

        RetryItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class LoadingItemViewHolder extends RecyclerView.ViewHolder {

        LoadingItemViewHolder(View itemView) {
            super(itemView);
        }
    }

    public interface Presenter {
        TVShow getShowByPosition(int position);

        int getItemCount();
    }

    public interface OnItemClickListener {
        void onTVShowClick(long id);

        void onRetryButtonClick();
    }

    private enum ItemType {
        NONE, TV_SHOW, LOADING_SPINNER, RETRY_BUTTON;

        private int ViewType;

        static {
            TV_SHOW.ViewType = 1;
            LOADING_SPINNER.ViewType = 2;
            RETRY_BUTTON.ViewType = 3;
        }
    }
}