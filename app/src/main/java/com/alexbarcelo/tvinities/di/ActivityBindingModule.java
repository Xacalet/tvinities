package com.alexbarcelo.tvinities.di;

import com.alexbarcelo.tvinities.popularTVShows.PopularTVShowsActivity;
import com.alexbarcelo.tvinities.popularTVShows.PopularTVShowsModule;
import com.alexbarcelo.tvinities.tvShowDetails.TVShowDetailsActivity;
import com.alexbarcelo.tvinities.tvShowDetails.TVShowDetailsModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = PopularTVShowsModule.class)
    abstract PopularTVShowsActivity showListActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = TVShowDetailsModule.class)
    abstract TVShowDetailsActivity showDetailActivity();
}
