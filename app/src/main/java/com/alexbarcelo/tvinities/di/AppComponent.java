package com.alexbarcelo.tvinities.di;

import android.app.Application;

import com.alexbarcelo.tvinities.TvinitiesApplication;
import com.alexbarcelo.commons.network.NetworkModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * Dagger component that injects the module dependencies on each activity
 *
 * @author Alex Barceló
 * @date 14/03/2018
 */

@Singleton
@Component(modules = {
        AppModule.class,
        NetworkModule.class,
        ActivityBindingModule.class,
        AndroidSupportInjectionModule.class})
public interface AppComponent extends AndroidInjector<TvinitiesApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        AppComponent.Builder application(Application application);

        AppComponent build();
    }
}