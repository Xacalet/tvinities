package com.alexbarcelo.tvinities.tvShowDetails;

import android.graphics.drawable.Drawable;

import com.alexbarcelo.tvinities.BasePresenter;
import com.alexbarcelo.tvinities.BaseView;

public interface TVShowDetailsContract {

    interface View extends BaseView<TVShowDetailsContract.Presenter> {
        void displayTVShowDetails(Long id);

        void showErrorDialog(String message);

        void showErrorMessage(String message);

        void hideErrorMessage();

        void refreshRelatedShowsList();

        void setName(String name);

        void setRating(String rating);

        void setRatingBackgroundColor(int color);

        void setOverview(String overview);

        void setGenres(String genres);

        void setYearAndSeasons(String yearAndSeasons);

        void setBackdropImage(Drawable image);

        void showRetryButtonOnRelatedTVShowsList();

        void showLoadingSpinnerOnRelatedTVShowsList();

        void hideRetryButtonOnRelatedTVShowsList();

        void hideLoadingSpinnerOnRelatedTVShowsList();
    }

    interface Presenter extends BasePresenter<TVShowDetailsContract.View> {

        void loadTVShowDetails(long id);

        void loadMoreRelatedTVShows(long id);

        void openTVShowDetails(Long id);
    }
}
