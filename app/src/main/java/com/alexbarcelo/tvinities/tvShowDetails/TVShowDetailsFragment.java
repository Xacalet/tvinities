package com.alexbarcelo.tvinities.tvShowDetails;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.alexbarcelo.tvinities.R;
import com.alexbarcelo.tvinities.adapters.TVShowListAdapter;
import com.alexbarcelo.tvinities.fragments.ErrorDialogFragment;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;

public class TVShowDetailsFragment extends DaggerFragment implements TVShowDetailsContract.View {

    public final static String ARGUMENT_SHOW_ID = "SHOW_ID";

    @Inject TVShowDetailsContract.Presenter mPresenter;
    @Inject TVShowListAdapter mRelatedShowsAdapter;

    private long mTVShowId;

    @BindView(R.id.error_text) TextView mErrorText;
    @BindView(R.id.error_view) ViewGroup mErrorView;
    @BindView(R.id.error_retry_button) Button mErrorRetryButton;
    @BindView(R.id.show_list_item_name) TextView mNameView;
    @BindView(R.id.activity_detail_backdrop_image) ImageView mBackdropView;
    @BindView(R.id.activity_detail_genres) TextView mGenresView;
    @BindView(R.id.activity_detail_year_and_seasons) TextView mYearsAndSeasonsView;
    @BindView(R.id.show_list_item_rating) TextView mRatingView;
    @BindView(R.id.activity_detail_overview) TextView mOverviewView;
    @BindView(R.id.activity_detail_scroll_view) ScrollView mScrollView;
    @BindView(R.id.activity_detail_progress_bar) ProgressBar mProgressBar;
    @BindView(R.id.related_shows_list) RecyclerView mRelatedShowsList;

    public static TVShowDetailsFragment newInstance(Long showId) {
        TVShowDetailsFragment fragment = new TVShowDetailsFragment();

        Bundle args = new Bundle();
        args.putLong(ARGUMENT_SHOW_ID, showId);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.mTVShowId = getArguments().getLong(ARGUMENT_SHOW_ID);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_show_detail, container, false);

        ButterKnife.bind(this, v);

        mRelatedShowsAdapter.setOnItemClickListener(new TVShowListAdapter.OnItemClickListener() {
            @Override
            public void onTVShowClick(long id) {
                mPresenter.openTVShowDetails(id);
            }

            @Override
            public void onRetryButtonClick() {
                mPresenter.loadTVShowDetails(mTVShowId);
            }
        });

        mRelatedShowsList.setAdapter(mRelatedShowsAdapter);
        mRelatedShowsList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        mRelatedShowsList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (layoutManager.findLastVisibleItemPosition() == mRelatedShowsAdapter.getItemCount() - 1 && !isErrorDisplayed()) {
                    mPresenter.loadMoreRelatedTVShows(mTVShowId);
                }
            }
        });

        mErrorRetryButton.setOnClickListener(view -> {
            hideErrorMessage();
            mPresenter.loadTVShowDetails(mTVShowId);
        });

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.takeView(this);
        mPresenter.loadTVShowDetails(this.mTVShowId);
    }


    @Override
    public void displayTVShowDetails(Long id) {
        Intent intent = new Intent(getActivity(), TVShowDetailsActivity.class);
        intent.putExtra(TVShowDetailsActivity.EXTRA_TV_SHOW_ID, id);
        startActivity(intent);
    }

    @Override
    public void showErrorDialog(String message) {
        ErrorDialogFragment dialogFragment = ErrorDialogFragment.newInstance(message);
        dialogFragment.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), null);
    }

    @Override
    public void showErrorMessage(String message) {
        mErrorText.setText(message);
        mErrorView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideErrorMessage() {
        mErrorView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void refreshRelatedShowsList() {
        mRelatedShowsAdapter.notifyDataSetChanged();
    }

    @Override
    public void setName(String name) {
        mNameView.setText(name);
    }

    @Override
    public void setRating(String rating) {
        mRatingView.setText(rating);
    }

    @Override
    public void setRatingBackgroundColor(int color) {
        mRatingView.setBackgroundColor(color);
    }

    @Override
    public void setOverview(String overview) {
        mOverviewView.setText(overview);
    }

    @Override
    public void setGenres(String genres) {
        mGenresView.setText(genres);
    }

    @Override
    public void setYearAndSeasons(String yearAndSeasons) {
        mYearsAndSeasonsView.setText(yearAndSeasons);
    }

    @Override
    public void setBackdropImage(Drawable image) {
        mBackdropView.setImageDrawable(image);
    }

    @Override
    public void showRetryButtonOnRelatedTVShowsList() {
        mRelatedShowsAdapter.showRetryButtonItem();
    }

    @Override
    public void showLoadingSpinnerOnRelatedTVShowsList() {
        mRelatedShowsAdapter.showLoadingSpinnerItem();
    }

    @Override
    public void hideRetryButtonOnRelatedTVShowsList() {
        mRelatedShowsAdapter.hideRetryButtonItem();
    }

    @Override
    public void hideLoadingSpinnerOnRelatedTVShowsList() {
        mRelatedShowsAdapter.hideRetryButtonItem();
    }

    private boolean isErrorDisplayed() {
        return mErrorView.getVisibility() == View.VISIBLE || mRelatedShowsAdapter.isRetryButtonDisplayed();
    }
}
