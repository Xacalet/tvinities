package com.alexbarcelo.tvinities.tvShowDetails;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.alexbarcelo.tvinities.R;
import com.alexbarcelo.tvinities.adapters.TVShowListAdapter;
import com.alexbarcelo.tvinities.api.TVinitiesAPI;
import com.alexbarcelo.tvinities.di.ActivityScoped;
import com.alexbarcelo.tvinities.glide.GlideApp;
import com.alexbarcelo.tvinities.model.Genre;
import com.alexbarcelo.tvinities.model.PaginatedList;
import com.alexbarcelo.tvinities.model.TVShow;
import com.alexbarcelo.tvinities.model.TVShowDetails;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.annotation.Nullable;
import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@ActivityScoped
public class TVShowDetailsPresenter implements TVShowDetailsContract.Presenter, TVShowListAdapter.Presenter {

    @Inject TVinitiesAPI mTVinitiesAPI;
    @Inject Context mContext;

    @Nullable private TVShowDetailsContract.View mView;

    private int mRelatedShowsCurrentPage = 0;
    private int mRelatedShowsPageCount = Integer.MAX_VALUE;
    private List<TVShow> mRelatedShows = new ArrayList<>();
    private boolean mIsLoadingRelatedShows = false;

    @Inject
    TVShowDetailsPresenter() { }

    @Override
    public void loadTVShowDetails(long id) {
        mTVinitiesAPI.getTVShowDetail(id).enqueue(new Callback<TVShowDetails>() {
            @Override
            public void onResponse(@NonNull Call<TVShowDetails> call, @NonNull Response<TVShowDetails> response) {
                TVShowDetails showDetails = response.body();
                if (response.isSuccessful() && showDetails != null) {
                    onLoadingDetailsSuccess(response.body());
                } else {
                    onLoadingDetailsError(response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<TVShowDetails> call, @NonNull Throwable t) {
                onLoadingDetailsError(t.getMessage());
            }
        });
    }

    @Override
    public void loadMoreRelatedTVShows(long id) {
        if (mIsLoadingRelatedShows || !canLoadMoreRelatedShows()) {
            return;
        }

        mIsLoadingRelatedShows = true;
        if (mView != null) {
            mView.showLoadingSpinnerOnRelatedTVShowsList();
        }

        mTVinitiesAPI.getSimilarShows(id, mRelatedShowsCurrentPage + 1).enqueue(new Callback<PaginatedList<TVShow>>() {
            @Override
            public void onResponse(@NonNull Call<PaginatedList<TVShow>> call, @NonNull Response<PaginatedList<TVShow>> response) {
                if (response.isSuccessful()) {
                    onLoadingMoreRelatedShowsSuccess(response.body());
                } else {
                    onLoadingMoreRelatedShowsError(response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<PaginatedList<TVShow>> call, @NonNull Throwable t) {
                onLoadingMoreRelatedShowsError(t.getMessage());
            }
        });
    }

    @Override
    public void openTVShowDetails(Long id) {
        if (mView != null) {
            mView.displayTVShowDetails(id);
        }
    }

    @Override
    public void takeView(TVShowDetailsContract.View view) {
        mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
    }

    @Override
    public TVShow getShowByPosition(int index) {
        return mRelatedShows.get(index);
    }

    @Override
    public int getItemCount() {
        return mRelatedShows.size();
    }

    private void onLoadingDetailsSuccess(TVShowDetails showDetails) {
        updateTVShowDetailsView(showDetails);
        loadMoreRelatedTVShows(showDetails.id());
    }

    private void updateTVShowDetailsView(TVShowDetails showDetails) {
        if (mView == null) {
            return;
        }

        //Update view
        mView.setName(showDetails.name());

        //Update backdrop image
        String backdropFullPath = "https://image.tmdb.org/t/p/w500" + showDetails.backdropPath();
        GlideApp.with(mContext)
                .load(backdropFullPath)
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @android.support.annotation.Nullable Transition<? super Drawable> transition) {
                        mView.setBackdropImage(resource);
                    }
                });

        //Update list of genres
        List<String> genreNames = new ArrayList<>();
        for (Genre g : showDetails.genres()) {
            genreNames.add(g.name());
        }
        String genres = TextUtils.join(", ", genreNames);
        mView.setGenres(genres);

        //Update start and end years
        String startYear = showDetails.firstAirDate().substring(0, 4);
        String endYear = showDetails.inProduction() ? mContext.getResources().getString(R.string.in_production) : showDetails.lastAirDate().substring(0, 4);
        int numberOfSeasons = showDetails.numberOfSeasons();
        String seasons = String.format(mContext.getResources().getQuantityString(R.plurals.numberOfSeasons, numberOfSeasons), numberOfSeasons);
        String yearsAndSeason = String.format("%s - %s | %s", startYear, endYear, seasons);
        mView.setYearAndSeasons(yearsAndSeason);

        //Update rating value
        Double voteRating = showDetails.voteAverage();
        String voteRatingFormatted = String.format(Locale.getDefault(), "%.1f", voteRating);
        mView.setRating(voteRatingFormatted);

        //Update rating color
        int color;
        if (voteRating >= 7.) {
            color = mContext.getResources().getColor(android.R.color.holo_green_dark);
        } else if (voteRating >= 5.) {
            color = mContext.getResources().getColor(android.R.color.holo_orange_dark);
        } else {
            color = mContext.getResources().getColor(android.R.color.holo_red_light);
        }
        mView.setRatingBackgroundColor(color);

        //Update overview text
        mView.setOverview(showDetails.overview());
    }

    private void onLoadingDetailsError(String errorMessage) {
        if (mView != null) {
            mView.showErrorMessage(errorMessage);
        }
    }

    private boolean canLoadMoreRelatedShows() {
        return this.mRelatedShowsCurrentPage < this.mRelatedShowsPageCount;
    }

    private void onLoadingMoreRelatedShowsSuccess(PaginatedList<TVShow> paginatedList) {
        if (paginatedList != null) {
            mRelatedShowsPageCount = paginatedList.totalPages();
            mRelatedShowsCurrentPage++;
            mRelatedShows.addAll(paginatedList.results());
            if (mView != null) {
                mView.hideLoadingSpinnerOnRelatedTVShowsList();
                mView.refreshRelatedShowsList();
            }
        }
        mIsLoadingRelatedShows = false;
    }

    private void onLoadingMoreRelatedShowsError(String errorMessage) {
        if (mView != null) {
            mView.hideLoadingSpinnerOnRelatedTVShowsList();
            mView.showErrorDialog(errorMessage);
            mView.showRetryButtonOnRelatedTVShowsList();
        }
        mIsLoadingRelatedShows = false;
    }
}