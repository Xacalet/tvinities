package com.alexbarcelo.tvinities.tvShowDetails;

import com.alexbarcelo.tvinities.adapters.TVShowListAdapter;
import com.alexbarcelo.tvinities.di.ActivityScoped;
import com.alexbarcelo.tvinities.di.FragmentScoped;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class TVShowDetailsModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract TVShowDetailsFragment showDetailFragment();

    @ActivityScoped
    @Binds
    abstract TVShowDetailsContract.Presenter showDetailPresenter(TVShowDetailsPresenter presenter);

    @ActivityScoped
    @Binds
    abstract TVShowListAdapter.Presenter showDetailAdapterPresenter(TVShowDetailsPresenter presenter);
}
