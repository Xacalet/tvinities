package com.alexbarcelo.tvinities.tvShowDetails;

import android.content.Intent;
import android.os.Bundle;

import com.alexbarcelo.tvinities.R;

import dagger.android.support.DaggerAppCompatActivity;

public class TVShowDetailsActivity extends DaggerAppCompatActivity {

    public static final String EXTRA_TV_SHOW_ID = "com.alexbarcelo.tvinities.TV_SHOW_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_detail);

        Intent intent = getIntent();
        Long showId = intent.getLongExtra(EXTRA_TV_SHOW_ID, -1);

        TVShowDetailsFragment TVShowDetailsFragment = (TVShowDetailsFragment) getSupportFragmentManager()
                .findFragmentById(R.id.content_frame);

        if (TVShowDetailsFragment == null) {
            TVShowDetailsFragment = TVShowDetailsFragment.newInstance(showId);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.content_frame, TVShowDetailsFragment)
                    .commit();
        }
    }
}
