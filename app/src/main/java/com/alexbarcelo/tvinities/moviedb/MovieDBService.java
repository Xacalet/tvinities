package com.alexbarcelo.tvinities.moviedb;

import com.alexbarcelo.tvinities.model.PaginatedList;
import com.alexbarcelo.tvinities.model.TVShowDetails;
import com.alexbarcelo.tvinities.model.TVShow;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Interface that provides a set of methods to access data from The Movie Database through its REST API
 *
 * @author Alex Barceló
 */

public interface MovieDBService {
    @GET("/3/tv/popular")
    Call<PaginatedList<TVShow>> getPopularTVShows(@Query("api_key") String apiKey, @Query("language") String language, @Query("page") int page);

    @GET("/3/tv/{tv_id}")
    Call<TVShowDetails> getTVShowDetail(@Path("tv_id") long id, @Query("api_key") String apiKey, @Query("language") String language);

    @GET("/3/tv/{tv_id}/similar")
    Call<PaginatedList<TVShow>> getSimilarShows(@Path("tv_id") long id, @Query("api_key") String apiKey, @Query("language") String language, @Query("page") int page);
}
