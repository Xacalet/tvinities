package com.alexbarcelo.tvinities.model;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.util.List;

@AutoValue
public abstract class PaginatedList<T> {

    @SerializedName("total_pages")
    public abstract int totalPages();

    public abstract List<T> results();

    public static <T> TypeAdapter<PaginatedList<T>> typeAdapter(Gson gson, TypeToken<? extends PaginatedList<T>> typeToken) {
        return new AutoValue_PaginatedList.GsonTypeAdapter(gson, typeToken);
    }
}
