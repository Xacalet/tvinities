package com.alexbarcelo.tvinities.model;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import javax.annotation.Nullable;

@AutoValue
public abstract class TVShowDetails {

    @SerializedName("backdrop_path")
    @Nullable
    public abstract String backdropPath();

    @SerializedName("first_air_date")
    public abstract String firstAirDate();

    public abstract List<Genre> genres();

    public abstract long id();

    @SerializedName("in_production")
    public abstract boolean inProduction();

    @SerializedName("last_air_date")
    public abstract String lastAirDate();

    public abstract String name();

    @SerializedName("number_of_seasons")
    public abstract int numberOfSeasons();

    public abstract String overview();

    public abstract String status();

    @SerializedName("vote_average")
    public abstract double voteAverage();

    public static TypeAdapter<TVShowDetails> typeAdapter(Gson gson) {
        return new AutoValue_TVShowDetails.GsonTypeAdapter(gson);
    }
}
