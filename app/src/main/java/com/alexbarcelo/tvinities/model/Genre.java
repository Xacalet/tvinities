package com.alexbarcelo.tvinities.model;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

@AutoValue
public abstract class Genre {

    public abstract String name();

    public static TypeAdapter<Genre> typeAdapter(Gson gson) {
        return new AutoValue_Genre.GsonTypeAdapter(gson);
    }
}
