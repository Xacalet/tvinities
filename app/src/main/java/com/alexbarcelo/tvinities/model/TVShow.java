package com.alexbarcelo.tvinities.model;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Nullable;

@AutoValue
public abstract class TVShow {

    public abstract String name();

    @SerializedName("vote_average")
    public abstract Double voteAverage();

    public abstract Long id();

    @SerializedName("poster_path")
    @Nullable
    public abstract String posterPath();

    public static TypeAdapter<TVShow> typeAdapter(Gson gson) {
        return new AutoValue_TVShow.GsonTypeAdapter(gson);
    }
}