package com.alexbarcelo.tvinities.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alexbarcelo.tvinities.R;

import java.util.Locale;

/**
 * A compound viewgroup that holds the view for an item from any TV Show list in the app
 *
 * @author Alex Barceló
 */

public class TVShowListItemView extends RelativeLayout {
    private LayoutInflater mInflater;

    private TextView mNameTextView;
    private TextView mRatingTextView;
    private ImageView mPosterImageView;

    public TVShowListItemView(Context context) {
        super(context);
        mInflater = LayoutInflater.from(context);
        init();
    }

    public TVShowListItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mInflater = LayoutInflater.from(context);
        init();
    }

    public void setName(String name) {
        mNameTextView.setText(name);
    }

    public void setRating(Double rating) {
        int GREEN_COLOR = 0xFF669900;
        int ORANGE_COLOR = 0xFFFF8800;
        int RED_COLOR = 0xFFFF4444;

        mRatingTextView.setText(String.format(Locale.getDefault(), "%.1f", rating));
        if (rating >= 7.) {
            mRatingTextView.setBackgroundColor(GREEN_COLOR);
        } else if (rating >= 5.) {
            mRatingTextView.setBackgroundColor(ORANGE_COLOR);
        } else {
            mRatingTextView.setBackgroundColor(RED_COLOR);
        }
    }

    public void setPoster(Bitmap poster) {
        mPosterImageView.setImageBitmap(poster);
    }

    public void init() {
        View v = mInflater.inflate(R.layout.tv_show_list_item, this, true);
        mNameTextView = v.findViewById(R.id.show_list_item_name);
        mRatingTextView = v.findViewById(R.id.show_list_item_rating);
        mPosterImageView = v.findViewById(R.id.show_list_item_poster);
    }
}
