package com.alexbarcelo.tvinities.popularTVShows;

import android.support.annotation.NonNull;

import com.alexbarcelo.tvinities.adapters.TVShowListAdapter;
import com.alexbarcelo.tvinities.api.TVinitiesAPI;
import com.alexbarcelo.tvinities.di.ActivityScoped;
import com.alexbarcelo.tvinities.model.PaginatedList;
import com.alexbarcelo.tvinities.model.TVShow;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@ActivityScoped
public class PopularTVShowsPresenter implements PopularTVShowsContract.Presenter, TVShowListAdapter.Presenter {

    @Inject TVinitiesAPI mTVinitiesAPI;

    @Nullable private PopularTVShowsContract.View mView;

    private int mCurrentPage = 0;
    private int mPageCount = Integer.MAX_VALUE;
    private List<TVShow> mShows = new ArrayList<>();
    private boolean mIsLoadingShows = false;

    @Inject
    PopularTVShowsPresenter() { }

    @Override
    public void loadMorePopularTVShows() {
        if (mIsLoadingShows || !canLoadMoreShows()) {
            return;
        }

        mIsLoadingShows = true;
        if (mView != null) {
            if (mCurrentPage == 0) {
                mView.showLoadingSpinner();
            } else {
                mView.showLoadingSpinnerOnList();
            }
        }

        mTVinitiesAPI.getPopularTVShows(mCurrentPage + 1).enqueue(new Callback<PaginatedList<TVShow>>() {
            @Override
            public void onResponse(@NonNull Call<PaginatedList<TVShow>> call, @NonNull Response<PaginatedList<TVShow>> response) {
                PaginatedList<TVShow> popularShows = response.body();
                if (response.isSuccessful() && popularShows != null) {
                    onLoadingMorePopularTVShowsSuccess(popularShows);
                } else {
                    OnLoadingMorePopularTVShowsError(response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<PaginatedList<TVShow>> call, @NonNull Throwable t) {
                OnLoadingMorePopularTVShowsError(t.getMessage());
            }
        });
    }

    @Override
    public void openTVShowDetails(Long id) {
        if (mView != null) {
            mView.displayTVShowDetails(id);
        }
    }

    @Override
    public void takeView(PopularTVShowsContract.View view) {
        mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
    }

    @Override
    public TVShow getShowByPosition(int index) {
        return mShows.get(index);
    }

    @Override
    public int getItemCount() {
        return mShows.size();
    }

    private boolean canLoadMoreShows() {
        return this.mCurrentPage < this.mPageCount;
    }

    private void onLoadingMorePopularTVShowsSuccess(PaginatedList<TVShow> paginatedList) {
        mCurrentPage++;
        mPageCount = paginatedList.totalPages();
        mShows.addAll(paginatedList.results());
        if (mView != null) {
            if (mCurrentPage == 1) {
                mView.hideLoadingSpinner();
            } else {
                mView.hideLoadingSpinnerOnList();
            }
            mView.refreshList();
        }
        mIsLoadingShows = false;
    }

    private void OnLoadingMorePopularTVShowsError(String errorMessage) {
        if (mView != null) {
            if (mCurrentPage == 0) {
                mView.hideLoadingSpinner();
                mView.showErrorMessage(errorMessage);
            } else {
                mView.hideLoadingSpinnerOnList();
                mView.showErrorDialog(errorMessage);
                mView.showRetryButtonOnList();
            }
        }
        mIsLoadingShows = false;
    }
}
