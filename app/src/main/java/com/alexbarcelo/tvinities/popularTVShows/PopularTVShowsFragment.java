package com.alexbarcelo.tvinities.popularTVShows;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alexbarcelo.tvinities.R;
import com.alexbarcelo.tvinities.adapters.TVShowListAdapter;
import com.alexbarcelo.tvinities.di.ActivityScoped;
import com.alexbarcelo.tvinities.fragments.ErrorDialogFragment;
import com.alexbarcelo.tvinities.tvShowDetails.TVShowDetailsActivity;
import com.sqisland.android.recyclerview.AutofitRecyclerView;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;

@ActivityScoped
public class PopularTVShowsFragment extends DaggerFragment implements PopularTVShowsContract.View {

    @Inject PopularTVShowsContract.Presenter mPresenter;
    @Inject TVShowListAdapter mPopularShowsListAdapter;

    @BindView(R.id.error_text) TextView mErrorText;
    @BindView(R.id.error_view) ViewGroup mErrorView;
    @BindView(R.id.error_retry_button) Button mErrorRetryButton;
    @BindView(R.id.loading_spinner) ProgressBar mLoadingSpinner;
    @BindView(R.id.popular_shows_list) AutofitRecyclerView mPopularShowsList;

    @Inject
    public PopularTVShowsFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_popular_tv_shows, container, false);

        ButterKnife.bind(this, v);

        //Tells presenter to load more TV shows if last item on adapter is visible.
        mPopularShowsList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                GridLayoutManager layoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
                int lastItemIndex = mPopularShowsListAdapter.getItemCount() - 1;
                if (layoutManager.findLastVisibleItemPosition() == lastItemIndex && !isErrorDisplayed()) {
                    mPresenter.loadMorePopularTVShows();
                }
            }
        });

        mPopularShowsListAdapter.setOnItemClickListener(new TVShowListAdapter.OnItemClickListener() {
            @Override
            public void onTVShowClick(long id) {
                mPresenter.openTVShowDetails(id);
            }

            @Override
            public void onRetryButtonClick() {
                mPresenter.loadMorePopularTVShows();
            }
        });

        mPopularShowsList.setAdapter(mPopularShowsListAdapter);

        mErrorRetryButton.setOnClickListener(view -> mPresenter.loadMorePopularTVShows());

        mPresenter.loadMorePopularTVShows();

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.takeView(this);
    }

    @Override
    public void displayTVShowDetails(Long id) {
        Intent intent = new Intent(getActivity(), TVShowDetailsActivity.class);
        intent.putExtra(TVShowDetailsActivity.EXTRA_TV_SHOW_ID, id);
        startActivity(intent);
    }

    @Override
    public void showErrorMessage(String message) {
        mErrorText.setText(message);
        mErrorView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideErrorMessage() {
        mErrorView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showErrorDialog(String message) {
        ErrorDialogFragment dialogFragment = ErrorDialogFragment.newInstance(message);
        dialogFragment.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), null);
    }

    @Override
    public void showLoadingSpinner() {
        mLoadingSpinner.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingSpinner() {
        mLoadingSpinner.setVisibility(View.INVISIBLE);
    }

    @Override
    public void refreshList() {
        mPopularShowsListAdapter.notifyDataSetChanged();
    }

    @Override
    public void showRetryButtonOnList() {
        mPopularShowsListAdapter.showRetryButtonItem();
    }

    @Override
    public void showLoadingSpinnerOnList() {
        mPopularShowsListAdapter.showLoadingSpinnerItem();
    }

    @Override
    public void hideRetryButtonOnList() {
        mPopularShowsListAdapter.hideRetryButtonItem();
    }

    @Override
    public void hideLoadingSpinnerOnList() {
        mPopularShowsListAdapter.hideLoadingSpinnerItem();
    }

    private boolean isErrorDisplayed() {
        return mErrorView.getVisibility() == View.VISIBLE || mPopularShowsListAdapter.isRetryButtonDisplayed();
    }
}
