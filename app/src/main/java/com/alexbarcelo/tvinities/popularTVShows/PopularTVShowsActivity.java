package com.alexbarcelo.tvinities.popularTVShows;

import android.os.Bundle;

import com.alexbarcelo.tvinities.R;

import dagger.android.support.DaggerAppCompatActivity;

public class PopularTVShowsActivity extends DaggerAppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_list);

        PopularTVShowsFragment popularTVShowsFragment = (PopularTVShowsFragment) getSupportFragmentManager()
                .findFragmentById(R.id.content_frame);

        if (popularTVShowsFragment == null) {
            popularTVShowsFragment = new PopularTVShowsFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.content_frame, popularTVShowsFragment)
                    .commit();
        }
    }
}