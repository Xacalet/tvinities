package com.alexbarcelo.tvinities.popularTVShows;

import com.alexbarcelo.tvinities.BasePresenter;
import com.alexbarcelo.tvinities.BaseView;
import com.alexbarcelo.tvinities.adapters.TVShowListAdapter;

public interface PopularTVShowsContract {

    interface View extends BaseView<Presenter> {
        void displayTVShowDetails(Long id);

        void showErrorMessage(String message);

        void hideErrorMessage();

        void showErrorDialog(String message);

        void showLoadingSpinner();

        void hideLoadingSpinner();

        void refreshList();

        void showRetryButtonOnList();

        void showLoadingSpinnerOnList();

        void hideRetryButtonOnList();

        void hideLoadingSpinnerOnList();
    }

    interface Presenter extends BasePresenter<View> {
        void loadMorePopularTVShows();

        void openTVShowDetails(Long id);
    }
}