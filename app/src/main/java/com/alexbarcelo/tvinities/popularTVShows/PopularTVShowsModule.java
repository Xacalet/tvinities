package com.alexbarcelo.tvinities.popularTVShows;

import com.alexbarcelo.tvinities.adapters.TVShowListAdapter;
import com.alexbarcelo.tvinities.di.ActivityScoped;
import com.alexbarcelo.tvinities.di.FragmentScoped;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class PopularTVShowsModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract PopularTVShowsFragment showListFragment();

    @ActivityScoped
    @Binds
    abstract PopularTVShowsContract.Presenter showListPresenter(PopularTVShowsPresenter presenter);

    @ActivityScoped
    @Binds
    abstract TVShowListAdapter.Presenter showListAdapterPresenter(PopularTVShowsPresenter presenter);
}
